const Task = require("./task");

/**
 * Tasks {
 *  _list: {
 *      '32cce31f-4e2b-426c-85fe-eaee0a06f3d1': Task {
 *          id: '32cce31f-4e2b-426c-85fe-eaee0a06f3d1',
 *          descripton: 'Comprar comida',
 *          completedIn: null
 *      }
 *  }
 * }
 */
class Tasks {
    _list = {};

    constructor() {
        this._list = {};
    }

    create(description = "") {
        const task = new Task(description);
        this._list[task.id] = task;
    }

    loadFromArray(tasks = []) {
        tasks.forEach((task) => {
            this._list[task.id] = task;
        });
    }

    get taskList() {
        const list = [];
        Object.keys(this._list).forEach((key) => list.push(this._list[key]));
        return list;
    }

    listAll() {
        this.taskList.forEach((task, index) => {
            const idx = `${index + 1}`.green;
            const { description, completedIn } = task;
            const status = completedIn ? "Completado".green : "Pendiente".red;
            console.log(`${idx} ${description} :: ${status}`);
        });
    }

    listByStatus(completed = true) {
        let index = 1;
        this.taskList.forEach((task) => {
            const { description, completedIn } = task;
            const status = completedIn ? "Completado".green : "Pendiente".red;
            if (completed) {
                if (completedIn) {
                    console.log(
                        `${
                            index.toString().green
                        } ${description} :: ${completedIn}`
                    );
                    index++;
                }
            } else {
                if (!completedIn) {
                    console.log(
                        `${index.toString().green} ${description} :: ${status}`
                    );
                    index++;
                }
            }
        });
    }

    deleteTask(id = "") {
        if (this._list[id]) {
            delete this._list[id];
        }
    }

    updateTaskByIds(ids = []) {
        // ids.forEach((id) => {
        //     const task = this._list[id];
        //     if (!task.completedIn) {
        //         task.completedIn = new Date().toISOString();
        //     }
        // });
        this.taskList.forEach((task) => {
            this._list[task.id].completedIn = !ids.includes(task.id)
                ? null
                : new Date().toISOString();
        });
    }
}

module.exports = Tasks;
