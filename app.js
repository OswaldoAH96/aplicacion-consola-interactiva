const {
    inquirerMenu,
    pause,
    readInput,
    taskMenu,
    confirm,
    taskMenuSelect,
} = require("./helpers/inquirer");
const { saveDB, readDB } = require("./helpers/saveToFile");
const Task = require("./models/task");
const Tasks = require("./models/tasks");

require("colors");

const main = async () => {
    let opt = "";
    const tasks = new Tasks();
    const tasksDB = readDB();
    if (tasksDB) {
        tasks.loadFromArray(tasksDB);
    }
    do {
        opt = await inquirerMenu();
        switch (opt) {
            case "1":
                //Crear
                const description = await readInput("Descripción:");
                tasks.create(description);
                break;
            case "2":
                tasks.listAll();
                break;
            case "3":
                tasks.listByStatus();
                break;
            case "4":
                tasks.listByStatus(false);
                break;
            case "5":
                const ids = await taskMenuSelect(tasks.taskList);
                tasks.updateTaskByIds(ids);
                break;
            case "6":
                const id = await taskMenu(tasks.taskList);
                if (id !== "0") {
                    if (await confirm("¿Está seguro?".yellow)) {
                        tasks.deleteTask(id);
                        console.log("Tarea borrada".blue);
                    }
                }
                break;
            default:
                break;
        }
        saveDB(tasks.taskList);
        if (opt !== "0") await pause();
    } while (opt !== "0");
};

main();
