const inquirer = require("inquirer");
require("colors");

const optionsMenu = [
    {
        type: "list",
        name: "option",
        message: "¿Qué desea hacer?",
        choices: [
            {
                value: "1",
                name: `${"1.".green} Crear tarea`,
            },
            {
                value: "2",
                name: `${"2.".green} Listar tareas`,
            },
            {
                value: "3",
                name: `${"3.".green} Listar tareas completadas`,
            },
            {
                value: "4",
                name: `${"4.".green} Listar tareas pendientes`,
            },
            {
                value: "5",
                name: `${"5.".green} Completar tarea(s)`,
            },
            {
                value: "6",
                name: `${"6.".green} Borrar tarea`,
            },
            {
                value: "0",
                name: `${"0.".green} Salir`,
            },
        ],
    },
];

const inquirerMenu = async () => {
    console.clear();
    console.log("===========================".green);
    console.log("Seleccione una opción".white);
    console.log("===========================".green);
    const { option } = await inquirer.prompt(optionsMenu);
    return option;
};
const pause = async () => {
    const question = [
        {
            type: "input",
            name: "enter",
            message: `Presione ${"enter".green} para continuar`,
        },
    ];
    console.log("\n");
    await inquirer.prompt(question);
};

const readInput = async (message = "") => {
    const question = [
        {
            type: "input",
            name: "description",
            message,
            validate(value) {
                if (value.length === 0) {
                    return "Por favor ingrese un valor";
                }
                return true;
            },
        },
    ];
    const { description } = await inquirer.prompt(question);
    return description;
};

const taskMenu = async (tasks = []) => {
    const choices = tasks.map((task, i) => {
        const index = `${i + 1}.`.green;
        return {
            value: task.id,
            name: `${index} ${task.description}`,
        };
    });
    choices.unshift({
        value: "0",
        name: "0".green + " Cancelar",
    });
    const optionsMenu = [
        {
            type: "list",
            name: "id",
            message: "¿Qué desea hacer?",
            choices,
        },
    ];
    const { id } = await inquirer.prompt(optionsMenu);
    return id;
};

const confirm = async (message) => {
    const question = [
        {
            type: "confirm",
            name: "ok",
            message,
        },
    ];
    const { ok } = await inquirer.prompt(question);
    return ok;
};

const taskMenuSelect = async (tasks = []) => {
    const choices = tasks.map((task, i) => {
        const index = `${i + 1}.`.green;
        return {
            value: task.id,
            name: `${index} ${task.description}`,
            checked: task.completedIn ? true : false,
        };
    });
    const optionsMenu = [
        {
            type: "checkbox",
            name: "ids",
            message: "Selecciones",
            choices,
        },
    ];
    const { ids } = await inquirer.prompt(optionsMenu);
    return ids;
};

module.exports = {
    inquirerMenu,
    pause,
    readInput,
    taskMenu,
    confirm,
    taskMenuSelect,
};
